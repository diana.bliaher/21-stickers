class StickersApi {
    static URL = 'https://62054479161670001741b708.mockapi.io/api/stickers';

    static request(config) {
        const conf = {
            uri: '',
            method: 'GET',
            data: null,
            error: 'API request error.',
            ...config,
        }

        return fetch(`${this.URL}/${conf.uri}`, {
            method: conf.method,
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
              },
            body: conf.data ? JSON.stringify(conf.data) : undefined,
        })
        .then((res) => {
            if (res.ok) {
              return res.json();
            }
    
            throw new Error(conf.error);
        });
    }

    static getList() {
        return this.request({error: 'Can not fetch stickers list'});
    }

    static create(sticker) {
        return this.request({method: 'POST', data: sticker, error: `Can not create sticker.`});
    }

    static update(id, sticker) {
        return this.request({ uri: id, method: 'PUT', data: sticker, error: `Can not update sticker.` });
    }

    static delete(id) {
        return this.request({ uri: id, method: 'DELETE', error: `Can not delete sticker.` });
    }
}

export default StickersApi;