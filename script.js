import StickersApi from "./StickersApi.js";

const descriptionAreaSelector = '#description';
const delBtnSelector = '.delete';

const $addBtn = $('#createBtn');
const $stickerBody = $('#stickersList');
const $stickerTemplate = $('#stickerTemplate').html();
const $stickersList = $('#stickersList');


$addBtn.on('click', onAddBtnClick);
$stickersList.on('focusout', descriptionAreaSelector, onDescriptionAreaOut);
$stickersList.on('click', delBtnSelector, onDelBtnClick);


StickersApi.getList()
    .then(renderStickersList)
    .catch(showError);

function onAddBtnClick(e) {
    e.preventDefault();
    const sticker = getDesc();

    StickersApi.create(sticker)
        .then(addStickerItem)
        .catch(showError);
}

function onDescriptionAreaOut(e) {
    const stickerDesc = getDesc($(e.target));
    const id = getSticker($(e.target)).data('id');

    StickersApi.update(id, stickerDesc)
        .catch(showError);
}

function onDelBtnClick(e) {
    const targetSticker = getSticker($(e.target));
    const id = targetSticker.data('id');

    StickersApi.delete(id)
        .then(targetSticker.remove())
        .catch(showError);
}

function getSticker(elem) {
    return elem.closest('.sticker');
}

function getDesc(elem=null) {

    const description = {
        description: elem ? elem.val() : '',
    }
    return description;
}

function renderStickersList(data) {
    const html = data.map(generateStickerHtml).join('');

    $stickerBody.html(html);
}

function addStickerItem(data) {
    const stickerItem = generateStickerHtml(data);
    $stickerBody.append(stickerItem);
}

function generateStickerHtml(data) {
    return $stickerTemplate
        .replace('{description}', data.description)
        .replace('{id}', data.id);
}

function showError(e) {
    alert(e.message);
}